<html>
    <head>
        <title>ACP Brevets</title>
    </head>

    <body>
        <h1>List of Brevet Controls</h1>

        <form action="" method="POST">
            <!-- User can choose open, close, or all -->
            <label for="all_open_close"> All, Open, or Close </label>
            <select name="all_open_close">
                <option value="all"> Open and Close </option>
                <option value="open"> Open Only </option>
                <option value="close"> Close Only </option>
            </select>

            <br>

            <!-- User can choose number of controls to show -->
            <label for="top"> Displayed Controls </label>
            <select name="top">

                <!-- Can display up to 20 controls, user has 20 options (1 to 20) -->
                <?php for ($i=1; $i<=20; $i++) { ?>
                    <option value="<?php echo $i;?>"> <?php echo$i;?> </option>
                <?php } ?>
            </select>
        
        <input type="submit" name="button" vaue="submit"/> </form>

            <?php

            # Only show controls if options have been submitted
            if (isset($_POST["all_open_close"])) {

                # Retrieve Variables
                $option = $_POST["all_open_close"];
                $top = $_POST["top"];
            
                # Show open and close
                if ($option == 'all') {
                    echo "<h3> Opening and Closing Times </h3>";
                    echo "<ul>";
                    # String together the request url
                    $request = 'http://laptop-service:5000/listAll' . '/json?top=' . $top;
                    $json = file_get_contents($request);
                    $obj = json_decode($json);
                    $open_time = $obj->open_time;
                    $close_time = $obj->close_time;

                    echo "Open Times:\n";
                    foreach ($open_time as $l) {
                        echo "<li> $l </li>";
                    }

                    echo "<br>";

                    echo "Close Times:\n";
                    foreach ($close_time as $l) {
                        echo "<li> $l </li>";
                    }

                    echo "</ul>";
                }

                # Show open only
                if ($option == 'open') {
                    echo "<h3> Opening Times Only </h3>";
                    echo "<ul>";
                    # String together the request url
                    $request = 'http://laptop-service:5000/listOpenOnly' . '/json?top=' . $top;
                    $json = file_get_contents($request);
                    $obj = json_decode($json);
                    $open_time = $obj->open_time;

                    echo "Open Times:\n";
                    foreach ($open_time as $l) {
                        echo "<li> $l </li>";
                    }

                    echo "</ul>";
                    
                }

                # Show close only
                if ($option == 'close') {
                    echo "<h3> Closing Times Only </h3>";
                    echo "<ul>";
                    # String together the request url
                    $request = 'http://laptop-service:5000/listCloseOnly' . '/json?top=' . $top;
                    $json = file_get_contents($request);
                    $obj = json_decode($json);
                    $close_time = $obj->close_time;

                    echo "Close Times:\n";
                    foreach ($close_time as $l) {
                        echo "<li> $l </li>";
                    }

                    echo "</ul>";
                }
            }

            ?>
    </body>
</html>
