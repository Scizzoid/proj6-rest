# Brevet time calculator

This program is designed to calculate control times for ACP brevets.

## ACP control times

Controls are points where a rider must obtain proof of passage, and control times are the minimum and maximum times by which the rider must arrive at the location.

Below is a table of the ranges of control distances and their associated minimum and maximum speeds. The possible control locations can range from 0 to 1000km.

|Control location (km)|Minimum Speed (km/hr)|Maximum Speed (km/hr)|
|---------------------|---------------------|---------------------|
|0 - 200              |15                   |34                   |
|200 - 400            |15                   |32                   |
|400 - 600            |15                   |30                   |
|600 - 1000           |11.428               |28                   |

## Calculating the opening and closing times

Take the distance of a specific control, divide it by the maximum speed for the opening time, and the minimum speed for the closing time. If your control spans further than one range shown above, you must calculate each range individually and combine them for the opening and closing times.

For example: Consider a 300km brevet with controls at 50km, 100km, 200km, and at the finish(300km).

## Opening Times

The controls at 50km, 100km, and 200km are each associated with a maximum speed of 34 km/H

50/34 = 1.47 (1.47 Hours = 1 Hour and 0.47 * 60 inutes. --> 1H28)

100/34 = 2.94 (2H54)

200/34 = 5.88 (5H53)

The control at 300km however, is associated with a maximum speed of 32 km/H. It also must take into account the maximum speed of the distance range before it.

200/34 = 5H53

300 - 200 = 100

100/32 = 3.125 (3H8)

5H35 + 3H8 = 8H43

## Closing Times

In this case, all controls (50km, 100km, 200km, 300km) are associated with a minimum speed of 15km/hr
50/15 = 3.33 (3H20)
100/15 = 6.66 (6H40)
200/15 = 13.33 (13H20)
300/15 = 20 (20H)

The starting point control (at 0km) will always close at one hour from the starting time.

There are also time limits for each brevet distance: 13H30 for 200km, 20H for 300km, 27H for 400km, 40H for 600km, and 75H for 1000km. These are all considered the closing time of the final control.

# Contact

Sterling Stewart

sstewar9@uoregon.edu
